import Vue from 'vue'
import Vuex from 'vuex'
import {Toast} from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(Vuex)
/* eslint-disable */
var store = new Vuex.Store({
  state: {
    userFilms: [],
    city: '',
    userId: '',
    cinema: []
  },
  getters: {
    userFilms (state) {
      console.log(state.userFilms)
      var ar = state.userFilms.filter(item => item)
      return ar
    },
    city (state) {
      return state.city
    }
  },
  mutations: {
    /**
     * 添加减少电影
     * @param {*} state 
     * @param {Object} payload 用户添加的电影信息对象
     */
    addFilm (state, payload) {
        var filmId = payload.filmId
        var name = payload.name
        var poster = payload.poster
        var premiereAt = payload.premiereAt
        var runtime = payload.runtime
        var number = parseInt(payload.number) || 1
        // 如果这个电影存在于购物车里面
        for (var i = 0; i < state.userFilms.length; i++) {
          if (state.userFilms[i].filmId === filmId) {
            var filmIndex = i
            var isExit = true
            break
          }
        }
        if (isExit) {
          console.log(1)
          state.userFilms[filmIndex].number += number
        } else {
          state.userFilms.push({
            filmId: payload.filmId,
            name: name,
            poster: poster,
            premiereAt: premiereAt,
            runtime: runtime,
            number: number
          })
        }
        // 当电影的数量减少为零时，将这个电影信息删除
        for (var i = 0; i < state.userFilms.length; i ++){
          if (state.userFilms[i].number === 0) {
            state.userFilms.splice(i, 1)
          }
        }
        var obj;
        if (number > 0) {
          obj ={
            message: '添加商品成功',
            position: 'moddle',
            duration: 2000
          }
        } else {
          obj ={
            message: '减少商品成功',
            position: 'moddle',
            duration: 2000
          }
        }
        Toast()
        localStorage.setItem('userShop', JSON.stringify(state.userFilms))
    },
    /**
     * 改变电影数量，当用户在购物车中的input框中直接输入数量的时候改变相应的电影票数
     */
    changeFilm (state, payload) {
      var filmId = payload.filmId
      var number = parseInt(payload.number)
      state.userFilms.forEach((item, index) => {
        if(filmId === item.filmId){
          if(number > 0){
            state.userFilms[index].number = number
          } else {
            state.userFilms.splice(index, 1)
          }
        }
      })
      var str = JSON.stringify(state.userFilms)
      localStorage.setItem('userShop', str)
    },
    /**
     * 用来改变用户选择的城市
     * @param {*} state 
     * @param {String} payload 用户传入的城市
     */
    selectCity (state,payload) {
      state.city = payload  
    },
    setShopCar (state, payload) {
      console.log(payload)
      state.userFilms = payload
    },
    changeUserId (state, payload) {
      console.log(payload)
      state.userId = payload
    },
    changeCinema (state, payload) {
      state.cinema = payload
    }
  }
})
export default store