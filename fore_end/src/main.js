// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from '@/router'
import store from './store'
import {Indicator} from 'mint-ui'
import '@/styles/common/px2rem.scss'
import '@/styles/common/resize.scss'
import '@/styles/base.scss'
Vue.use(Indicator)
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
