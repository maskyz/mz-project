import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
Vue.use(VueRouter)
var router = new VueRouter({
  routes: [
    {
      path: '/',
      component: () => import('@/views/home'),
      redirect: '/film/nowPlaying',
      children: [
        {
          path: 'film',
          component: () => import('@/components/film'),
          redirect: '/film/nowPlaying',
          children: [
            {
              path: 'nowPlaying',
              component: () => import('@/components/film/nowPlaying')
            },
            {
              path: 'commingSoon',
              component: () => import('@/components/film/commingSoon')
            }
          ]
        },
        {
          path: 'cinema',
          component: () => import('@/components/cinema')
        },
        {
          path: 'pintuan',
          component: () => import('@/components/pintuan')
        },
        {
          path: 'center',
          component: () => import('@/components/center')
        }
      ]
    },
    {
      path: '/films/:detail',
      component: () => import('@/views/detail')
    },
    {
      path: '/city',
      component: () => import('@/views/city'),
      beforeEnter (to, from, next) {
        next({
          query: {
            redirect: to.fullPath
          }
        })
      }
    },
    {
      path: '/user',
      component: () => import('@/components/center/user'),
      children: [
        {
          path: 'login',
          component: () => import('@/components/center/user/login'),
          beforeEnter (to, from, next) {
            var user = localStorage.getItem('user')
            if (user) {
              next('/user/set')
            } else {
              next()
            }
          }
        },
        {
          path: 'maizuo',
          component: () => import('@/components/center/user/maizuo'),
          beforeEnter (to, from, next) {
            // 从localStorage 中获取用户的信息
            var user = localStorage.getItem('user')
            if (user) {
              next()
            } else {
              next({
                path: '/user/login',
                query: {
                  redirect: to.fullPath
                }
              })
            }
          }
        },
        {
          path: 'yue',
          component: () => import('@/components/center/user/yue'),
          beforeEnter (to, from, next) {
            var user = localStorage.getItem('user')
            if (user) {
              next()
            } else {
              next({
                path: '/user/login',
                query: {
                  redirect: to.fullPath
                }
              })
            }
          }
        },
        {
          path: 'set',
          component: () => import('@/components/center/user/set'),
          beforeEnter (to, from, next) {
            var user = localStorage.getItem('user')
            if (user) {
              next()
            } else {
              next({
                path: '/user/login',
                query: {
                  redirect: to.fullPath
                }
              })
            }
          }
        },
        {
          path: 'shopCar',
          component: () => import('@/components/center/user/shopCar'),
          beforeEnter (to, from, next) {
            var user = localStorage.getItem('user')
            if (user) {
              next()
            } else {
              next({
                path: '/user/login',
                query: {
                  redirect: to.fullPath
                }
              })
            }
          }
        }
      ]
    },
    {
      path: '/cinemaSearch',
      component: () => import('@/components/cinema/search')
    },
    {
      path: '/cinemaDetail',
      component: () => import('@/components/cinema/detail')
    },
    {
      path: '*',
      redirect: '/film/nowPlaying'
    }
  ]
})
// 在全局前置路由将 localStorage 中获取到的数据重新存储到 store 中的状态中，避免
// 因刷新页面而使得 store 中的数据丢失，这里是在路由文件中引入了一个 store.js 文件
// 用于操作状态
// 在全局前置路由中这样设置的好处是不用在每个组件中都设置一遍
router.beforeEach((to, from, next) => {
  var obj = JSON.parse(localStorage.getItem('userShop'))
  var cinema = JSON.parse(localStorage.getItem('cinema'))
  var user = JSON.parse(localStorage.getItem('user'))
  if (obj) {
    store.commit('setShopCar', obj)
  }
  if (cinema) {
    store.commit('changeCinema', cinema)
  }
  if (user) {
    store.commit('changeUserId', user)
  }
  next()
})
export default router
