var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// 电影路由接口
var filmRouter = require('./router/film.js');
// 位置信息定位接口
var addressRouter = require('./router/address.js');
// 用户相关接口
var userRouter = require('./router/user.js');
// 影院相关接口
var cinemaRouter = require('./router/cinema.js');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/film',filmRouter);
app.use('/address',addressRouter);
app.use('/user',userRouter);
app.use('/cinema',cinemaRouter);
app.use('*',function (req,res,next) {
  res.send('404 Error');  
})
app.listen(3000);
console.log('服务启动成功')