var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var async = require('async'); 
var url = 'mongodb://127.0.0.1:27017';
// 获取电影信息列表
router.get('/list',function (req,res,next) {
  var filmType = parseInt(req.query.filmType) || 1;
  var page = parseInt(req.query.page) || 1;
  var pageSize = parseInt(req.query.pageSize) || 5;
  var result = {}; // 返回给前端的信息
  MongoClient.connect(url, { useNewUrlParser: true }, function(error, client){
      if(error){    // 如果数据库连接失败
         console.log('数据库连接失败');
         result.status = 1;
         result.msg = '网络连接失败';
         res.send(result);
         return; 
      }
      // 如果数据库连接成功
      var db = client.db('maizuo');
      var params = null;
      // 区分是正在上映还是即将上映
      if(filmType === 1){
        params = {
          premiereAt: { $lte: parseInt(new Date().getTime()/1000) }
        }
      } else {
        params = {
            premiereAt: { $gte: parseInt(new Date().getTime()/1000) }
        }
      }
      async.waterfall([
        function (cb) {
            db.collection('films').find(params).count(function(error,num){
                if(error){
                    console.log('数据条数查询失败');
                    cb(error);
                } else {
                    cb(null,num);
                }
            });
        },
        function (num,cb) {
            db.collection('films').find(params).skip(pageSize*(page-1)).limit(pageSize).toArray(function(error,data){
                if(error){
                    console.log('数据查询失败');
                    cb(error);
                } else {
                    cb(null,{ num, data});
                }
            });
        }
      ],function (error,results) {
        if(error){
            console.log(error);
            result.status = 1;
            result.msg = '网络错误'
        } else {
            result.status = 0;
            result.msg = 'ok';
            result.data = {
                films: results.data
            },
            result.totalSize = results.num;
        }
        // 将数据信息返回给前端
        res.send(result);
        client.close();
      });  
  })  

});
// 电影详情接口
router.get('/details',function(req,res,next){
    var result = {}; // 返回给前端的信息
    if(!req.query.filmId){
        result.msg = '操作有误';
        result.status = 1;
        res.json(result);
        return;
    }
    // 如果用户正确传递了参数
    // 连接数据库进行查询操作
    MongoClient.connect(url, { useNewUrlParser: true }, function(error,client){
        if(error){  // 如果数据库连接失败
            console.log('数据库连接失败');
            result.msg = '网络有误，请稍后重试';
            result.status = 1;
            res.json(result);
            return;
        }
        // 数据库连接成功
        var db = client.db('maizuo');
        db.collection('films').find({
            filmId: parseInt(req.query.filmId)
        }).toArray(function(error,data){
            if(error){
                console.log('数据库查询失败');
                result.msg = '网络连接失败，请稍后重试';
                result.status = 1;
            } else {
                result.msg = 'ok'
                result.data = data;
                result.status = 0;
            }
            //  关闭数据库，将数据返回给前端
            res.json(result);
            client.close();
        });
    });
});

module.exports = router;