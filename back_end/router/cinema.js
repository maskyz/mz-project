var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://127.0.0.1:27017';
router.get('/list',function(req, res, next){
  var result = {};  // 返回给前端的信息   
  MongoClient.connect(url, {useNewUrlParser: true}, function(error, client){
    if (error) {
      result.msg = '网络问题';
      result.status = 1;
      res.json(result);
      return;
    }
    // 如果正常连接数据库
    var db = client.db('maizuo');
    db.collection('cinema').find().toArray(function(error, data){
        if(error){
            console.log('数据库查询失败');
            result.msg = '网络错误';
            result.status = 1;
        } else {
            result.msg = 'ok';
            result.status = 0;
            result.data = data;
        }
        res.json(result);
        client.close();
    })
  });
});
/**
 * 获取电影放映场次
 */
router.get('/detail', function (req, res, next) {
  var result = {};   // 返回给前端的信息 
  MongoClient.connect(url, {useNewUrlParser: true}, function(error, client) {
    if (error) {
      result.msg = '网络错误';
      result.status = 1;
      res.json(result);
      return;
    }
    // 如果说没有错误
    var db = client.db('maizuo');
    db.collection('cinemadetail').find().toArray(function(error, data){
      if(error){
        result.msg = '网络错误';
        result.status = 1;
      }
      else{
        result.msg = 'ok';
        result.status = 0;
        result.data = data; 
      }
      res.json(result);
      client.close();
    })
  })
});
module.exports = router;