var express = require('express');
var router = express.Router();
var async = require('async');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var url = 'mongodb://127.0.0.1:27017';
router.get('/getYzm',function(req, res, next){
  var phone = req.query.phone;
  var result = {}; // 返回给前端的信息  
  console.log(phone); 
  if(!phone){
    result.msg = '非法操作';
    result.status = 1;
    res.json(result);
    return;
  }
  // 如果用户传递了正确的信息 
  var yzm = '';  
  for (var i = 0; i < 6; i ++){
    yzm += String(Math.ceil(Math.random()*10));  
  }
  // 将我们的手机号和验证码信息写入我们的数据库
  MongoClient.connect(url, { useNewUrlParser: true }, function (error,client) {
    if(error){  // 如果数据库连接失败
        console.log('数据库连接失败');
        result.status = 1;
        result.msg = '网络错误，请稍后重试';
        res.json(result);
        return;
    }
    // 如果数据库连接成功
    var db = client.db('maizuo');
    async.waterfall([
        function (cb) {
            db.collection('user').find({
                phone: phone 
            }).count(function (error, num){
                if(error){
                    console.log('数据条数查询失败');
                    cb(error);
                } else if(num < 1) {
                    cb(null, 'false');
                } else {
                    cb(null, 'true')
                }
            })
        },
        function (flag,cb) {
            // 如果这个用户已经存在
            if(flag == 'true'){
                db.collection('user').updateOne({
                    phone: phone
                },{
                    $set: {
                        yanzhengma: yzm
                    }
                },
                 function (error) {
                     if (error) {
                         cb(error);
                     } else {
                         cb(null)
                     }
                 })
            } else {
                db.collection('user').insertOne({
                    phone: phone,
                    yanzhengma: yzm,
                    money: 0
                },function (error){
                    if(error){
                        console.log('数据插入失败')
                        cb(error);
                    } else {
                        cb(null)
                    }                    
                });
            }
        }
    ],function(error, results){
        if(error){
            console.log(error);
            result.msg = '网络错误';
            result.status = 1;
        } else {
            result.msg = 'ok';
            result.status = 0;
            result.yzm = yzm;
        }
        var randomTime = Math.ceil(Math.random()*(30000-5000)+5000);
        // 一段时间将信息返回给前端
        setTimeout(function() {
          res.json(result); 
          client.close();
        },randomTime);
    })
  })

});
router.post('/login', function (req, res, next) {
  var phone = req.body.params.phone;
  var yzm = req.body.params.yzm;  
  var result = {}; // 返回给前端的信息
  if (!phone || !yzm) {
    result.msg = '操作有误';
    result.status = 1;
    res.json(result);
    return;
  }
   // 如果用户输入的表单是完整的
   // 那么就连接数据库对用户的手机号和验证码进行验证
   MongoClient.connect(url, {useNewUrlParser: true}, function(error, client){
      if(error){    // 如果数据库连接失败
          result.msg = '网络错误';
          console.log('数据库连接失败');
          result.status = 1;
          res.json(result);
          return;
      }  
      // 如果数据库连接成功
      var db = client.db('maizuo');
      db.collection('user').find({
          phone: phone,
          yanzhengma: yzm
      }).toArray(function(error,data){
          if(error){
              console.log('数据库查询失败');
              result.msg = '网络错误';
              result.status = 1;
          } else if(data.length < 1) {
            console.log('验证码错误');
            result.msg = '验证码错误'
            result.status = 1;
          }
          else {
              result.msg = 'ok';
              result.status = 0;
              result.userId = data[0]['_id'];
          }
          // 将数据返回给前端，关闭数据库
          res.json(result);
          client.close();
      })
   })
});
router.post('/set', function(req, res, next){
  // 用户传递过来的id值  
  var id = req.body.params.userId.replace(/"/g, '');
  console.log(id)
  // 返回给前端的信息
  var result = {};
  // 如果说用户没有传递过来这个id
  if (!id) {
    result.msg = '网络错误';
    result.status = 1;
    res.json(result);
    return;
  }
  // 如果用户将信息正确传递过来
  MongoClient.connect(url, {useNewUrlParser: true}, function(error, client){
      if (error) {  // 如果数据库连接失败
        result.msg = '网络错误';
        result.status = 1;
        res.json(result);
        return;
      }
      // 如果数据库连接成功
      var db = client.db('maizuo');
      db.collection('user').find({
        _id: ObjectId(id)  
      }).toArray(function(error,data){
          // 如果查找用户的信息失败
          if(error){
            result.msg = '未知错误';
            result.status = 1;
          } else if(data.length < 1){  // 如果没找到信息
            result.msg = '用户不存在';
            result.status = 1;
          } else {  // 如果找到了用户信息
            result.msg = 'ok';
            result.status = 0;
            result.data = data;
          }
          // 将信息返回给前端
          res.json(result);
          client.close();
      })
  })
});
router.post('/mony', function(req, res, next){
   var result = {};  // 返回给前端的信息
   console.log(id, req.body);
   var id = req.body.userId;
   console.log(id, req.body);
   var money = parseFloat(req.body.money);
   console.log(money)
   if(!id) { // 如果用户没有传递id
     result.msg = '请先登录';
     console.log('用户未传递参数');
     result.status = 1;
     res.json(result);
     return;  
   } 
   // 如果用户正确传递了id
   MongoClient.connect(url,{ useNewUrlParser:true}, function(error, client){
       if(error){ // 如果数据库连接失败
            result.msg = '网络错误';
            console.log("数据库连接失败");
            result.status = 1;
            res.json(result);
            return;
       }
       // 如果数据库连接成功
       var db = client.db('maizuo');
       // 如果用户传递了金额，说明用户是要充值
       if (money) {
            async.series([
                function(cb){
                    db.collection('user').updateOne({
                        _id: ObjectId(id)  
                      },{
                        $inc: {
                          money: money
                        }  
                      },function(error){
                          if(error){
                              cb(error);
                              console.log('数据插入失败');  
                          } else {
                              cb(null);
                          }
                      })  
                },
                function(cb){
                    db.collection('user').find({
                        _id: ObjectId(id)
                    }).toArray(function(error, data){
                        if(error){
                            cb(error);
                        } else {
                            cb(null, data);
                        }
                    })
                }
            ],function(error,results){
                if(error){
                    console.log(error);
                    result.msg = '网络错误';
                    result.status = 1;
                } else {
                    result.msg = 'ok';
                    result.status = 0;
                    result.data = results[1];
                }
                  // 将数据返回给前端
                res.json(result);
                client.close(); 
            })        
       } else { // 如果用户没有传递钱，就是获取余额
         db.collection('user').find({
           _id: ObjectId(id)  
         }).toArray(function(error, data){
             if(error){ //如果数据库查找失败
                result.status = 1;
                result.msg = '网络错误';
                console.log('数据查找失败');
             } else {
                 result.status = 0;
                 result.msg = 'ok';
                 result.data = data
             }
             // 将数据返回给前端
             res.json(result);
             client.close();
         })
       }
   })
})
module.exports = router;